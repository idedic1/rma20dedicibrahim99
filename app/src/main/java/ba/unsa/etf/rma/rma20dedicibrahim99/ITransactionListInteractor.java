package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;

public interface ITransactionListInteractor {
    ArrayList<Transaction> getTransaction();

    void deleteTransaction(Transaction t);

    void addTransaction(Transaction t);

    ArrayList<String> getTransactionType();

    ArrayList<String> getSortType();

    void modifikacija(Transaction transaction, Date date, Double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate);

    void setTransactions(ArrayList<Transaction> transactions);
}