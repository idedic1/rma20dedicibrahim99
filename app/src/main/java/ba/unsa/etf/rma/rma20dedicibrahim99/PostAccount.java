package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class PostAccount extends AsyncTask<String, Integer, Void> {
    private String id = "9a575e28-e19d-442b-b946-5ebad091e114";
    private String root = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private Account account;
    private OnPostAccountDone caller;

    public PostAccount(OnPostAccountDone caller) {
        this.caller = caller;
    }

    public interface OnPostAccountDone {
        public void onDone();
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone();
    }


    @Override
    protected Void doInBackground(String... strings) {
        String url1 = root + "/account/" + id;
        try {
            URL url = new URL(url1);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(5000);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestMethod("POST");
            String jsonInputString;
            if(strings[1].equals("budzet")){
                jsonInputString = "{\"budget\": " + strings[0] + "}";
            } else {
                jsonInputString = "{\"budget\": " + strings[0] + ", \"totalLimit\": " + strings[1] + ", \"monthLimit\": " + strings[2] + "}";
            }
            OutputStream os = con.getOutputStream();
            os.write(jsonInputString.getBytes("UTF-8"));
            os.close();
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
