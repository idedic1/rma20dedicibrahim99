package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM, yyyy");
    private int resource;
    public ImageView ikonicaIv;
    public TextView titleTv;
    public TextView iznosTv;
    private Transaction selectedTransaction;
    private ArrayList<Transaction> items;
    private ArrayList<Transaction> filterItems = new ArrayList<>();

    public TransactionListAdapter(@NonNull Context context, int resource, @NonNull List<Transaction> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    public Transaction getSelectedTransaction() {
        return selectedTransaction;
    }

    public void setSelectedTransaction(Transaction selectedTransaction) {
        this.selectedTransaction = selectedTransaction;
    }

    public void setTransaction(ArrayList<Transaction> transaction) {
        this.items = transaction;
    }

    public Transaction getTransaction(int position){
        return this.getItem(position);
    }

    boolean between(int i, int min, int max){
        return (i>= min && i<=max);
    }



    public void filterTransaction(String type, Calendar calendar) {
       int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR)-1900;
        if (type.equals("All")) {
            this.clear();
            filterItems.clear();
            for(Transaction x : items){
                if((x.getDate().getMonth() == month && x.getDate().getYear() == year )||
                        ((x.getType().toString().equals("Regular payment") || x.getType().toString().equals("Regular income"))
                                && between(month, x.getDate().getMonth(), x.getEndDate().getMonth()) &&
                                between(year, x.getDate().getYear(), x.getEndDate().getYear()))) filterItems.add(x);
             }
            this.addAll(filterItems);
            return;
        }
        if(type.equals("Regular payment") || type.equals("Regular income")){
            this.clear();
            filterItems.clear();
            for(Transaction x : items) {
                if (x.getType().toString().equals(type) && (x.getDate().getMonth() == month && x.getDate().getYear() == year ) || x.getType().toString().equals(type) && between(month, x.getDate().getMonth(), x.getEndDate().getMonth()) &&
                        between(year, x.getDate().getYear(), x.getEndDate().getYear())) filterItems.add(x);
            }
            this.addAll(filterItems);
            return;
        }
        filterItems.clear();
        for (Transaction x : items) {
            if (x.getType().toString().equals(type) && x.getDate().getMonth() == month && x.getDate().getYear() == year) filterItems.add(x);
        }
        this.clear();
        this.addAll(filterItems);
    }


    public void sortPricaASC(){
        Collections.sort(filterItems, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o1.getAmount().compareTo(o2.getAmount());
            }
        });
        this.clear();
        this.addAll(filterItems);
    }

    public void sortPricaDSC(){
        Collections.sort(filterItems, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o2.getAmount().compareTo(o1.getAmount());
            }
        });
        this.clear();
        this.addAll(filterItems);
    }

    public void sortTitleASC(){
        Collections.sort(filterItems, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
            }
        });
        this.clear();
        this.addAll(filterItems);
    }

    public void sortTitleDSC(){
        Collections.sort(filterItems, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o2.getTitle().toLowerCase().compareTo(o1.getTitle().toLowerCase());
            }
        });
        this.clear();
        this.addAll(filterItems);
    }

    public void sortDateASC(){
        Collections.sort(filterItems, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });
        this.clear();
        this.addAll(filterItems);
    }

    public void sortDateDSC(){
        Collections.sort(filterItems, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction o1, Transaction o2) {
                return o2.getDate().compareTo(o1.getDate());
            }
        });
        this.clear();
        this.addAll(filterItems);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }

        Transaction transaction = getItem(position);

        titleTv = newView.findViewById(R.id.textView4);
        iznosTv = newView.findViewById(R.id.textView5);
        ikonicaIv = newView.findViewById(R.id.imageView3);
        titleTv.setText(transaction.getTitle());
        iznosTv.setText(Double.toString(transaction.getAmount()));



        if(selectedTransaction == transaction){
            newView.setBackgroundColor(Color.LTGRAY);
        } else {
            newView.setBackgroundColor(Color.TRANSPARENT);
        }

        String typeMatch = transaction.getType().toString().replaceAll("\\s+","_").toLowerCase();
        try {
            Class res = R.drawable.class;
            Field field = res.getField(typeMatch);
            int drawableId = field.getInt(null);
            ikonicaIv.setImageResource(drawableId);
        }
        catch (Exception e) {
            ikonicaIv.setImageResource(R.drawable.ic_launcher_background);
        }

        return newView;
    }
}
