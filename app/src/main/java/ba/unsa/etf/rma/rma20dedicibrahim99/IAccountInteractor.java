package ba.unsa.etf.rma.rma20dedicibrahim99;

public interface IAccountInteractor {
    Account getAccount();
    void setAccount(Account results);
    void updateMonthLimit(Double limit);
    void updateTotalLimit(Double limit);
}
