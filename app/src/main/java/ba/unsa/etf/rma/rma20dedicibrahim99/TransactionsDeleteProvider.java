package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class TransactionsDeleteProvider extends ContentProvider {
    static final String PROVIDER_NAME = "ba.unsa.etf.rma.rma20dedicibrahim99transactionsdel.CP";
    static final String URL = "content://" + PROVIDER_NAME + "/transactions";
    static final Uri CONTENT_URI = Uri.parse(URL);
    static final String _ID = "_id";
    static final String ID = "id";
    static final String DATE = "date";
    static final String AMOUNT = "amount";
    static final String TITLE = "title";
    static final String TYPEID = "type";
    static final String ITEMDESCRIPTION = "itemDescription";
    static final String INTERVAL = "transactionInterval";
    static final String ENDDATE = "endDate";
    private static HashMap<String, String> TRANSACTIONS_PROJECTION_MAP;
    static final int TRANSACTIONS = 1;
    static final int TRANSACTION_ID = 2;
    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "transactions", TRANSACTIONS);
        uriMatcher.addURI(PROVIDER_NAME, "transactions/#", TRANSACTION_ID);
    }

    private SQLiteDatabase db;
    static final String DATABASE_NAME = "TransactionsDel";
    static final String TRANSACTIONS_TABLE_NAME = "transactionsDel";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE = " CREATE TABLE " + TRANSACTIONS_TABLE_NAME +
            " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " id TEXT, " +
            " date TEXT, " +
            " amount TEXT, " +
            " title TEXT, " +
            " type TEXT, " +
            " itemDescription TEXT, " +
            " transactionInterval TEXT, " +
            " endDate TEXT);";


    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TRANSACTIONS_TABLE_NAME);
            onCreate(db);
        }
    }

    @Override
    public boolean onCreate() {
        Context context = getContext();
        TransactionsDeleteProvider.DatabaseHelper dbHelper = new TransactionsDeleteProvider.DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null) ? false : true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TRANSACTIONS_TABLE_NAME);
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                qb.setProjectionMap(TRANSACTIONS_PROJECTION_MAP);
                break;
            case TRANSACTION_ID:
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == "") {
            sortOrder = _ID;
        }
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        // Pregled sadrzaja URI-a
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                return "vnd.android.cursor.dir/vnd.example.transactionsdel";
            case TRANSACTION_ID:
                return "vnd.android.cursor.item/vnd.example.transactionsdel";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        long rowID = db.insert(TRANSACTIONS_TABLE_NAME, "", values);
        // Ako je slog uspjesno dodan
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                count = db.delete(TRANSACTIONS_TABLE_NAME, selection, selectionArgs);
                break;
            case TRANSACTION_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete(TRANSACTIONS_TABLE_NAME, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case TRANSACTIONS:
                count = db.update(TRANSACTIONS_TABLE_NAME, values, selection, selectionArgs);
                break;
            case TRANSACTION_ID:
                count = db.update(TRANSACTIONS_TABLE_NAME, values, _ID + " = " + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
