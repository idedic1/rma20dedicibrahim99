package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TransactionDetailFragment extends Fragment {

    private EditText titleEt;
    private EditText amountEt;
    private EditText descriptionEt;
    private EditText intervalEt;
    private TextView dateTv;
    private TextView rezimTv;
    private TextView endDateTv;
    private Spinner typeSpinner;
    private Button saveBtn;
    private Button deleteBtn;

    private ArrayAdapter adapter;

    private DatePickerDialog.OnDateSetListener dateSetListener;
    private DatePickerDialog.OnDateSetListener endDateSetListener;

    private Date datum1;
    private Date datum2;

    Transaction transaction;

    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

    private ITransactionDetailPresenter presenter;

    public ITransactionDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new TransactionDetailPresenter(getActivity());
        }
        return presenter;
    }

    private boolean broj;

    private OnSaveClick onSaveClick;
    public interface OnSaveClick{
        public void onSaveClicked();
    }

    private OnDeleteClick onDeleteClick;
    public interface OnDeleteClick{
        public void onDeleteClicked();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction_detail, container, false);
        if (getArguments() != null && getArguments().containsKey("transaction")) {
            getPresenter().setTransaction(getArguments().getParcelable("transaction"));
            broj = getArguments().getBoolean("oznaka");
            titleEt = (EditText) view.findViewById(R.id.editText);
            amountEt = (EditText) view.findViewById(R.id.editText2);
            descriptionEt = (EditText) view.findViewById(R.id.editText3);
            intervalEt = (EditText) view.findViewById(R.id.editText4);
            dateTv = (TextView) view.findViewById(R.id.textView);
            endDateTv = (TextView) view.findViewById(R.id.textView7);
            typeSpinner = (Spinner) view.findViewById(R.id.spinner3);
            saveBtn = (Button) view.findViewById(R.id.button);
            deleteBtn = (Button) view.findViewById(R.id.button4);
            rezimTv = (TextView) view.findViewById(R.id.textView8);

            transaction = getPresenter().getTransaction();

            if(!broj) deleteBtn.setVisibility(View.GONE);

            String date2 = null;
            String date1 = null;
            if (transaction.getDate() != null) {
                date1 = format.format(transaction.getDate());
            }
            if (transaction.getEndDate() != null) {
                date2 = format.format(transaction.getEndDate());
            }

            titleEt.setText(transaction.getTitle());
            if (transaction.getAmount() != null) {
                amountEt.setText(transaction.getAmount().toString());
            } else {
                amountEt.setText("");
            }

            if (transaction.getItemDescription() != null) {
                descriptionEt.setText(transaction.getItemDescription());
            } else {
                descriptionEt.setText("");
            }

            if (transaction.getTransactionInterval() != null) {
                intervalEt.setText(transaction.getTransactionInterval().toString());
            } else {
                intervalEt.setText("");
            }
            if (date1 == null) {
                dateTv.setText(null);
            } else {
                dateTv.setText(date1);

            }
            if (date2 == null) {
                endDateTv.setText(null);
            } else {
                endDateTv.setText(date2);

            }

            ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                rezimTv.setVisibility(View.INVISIBLE);
            } else {
                if(transaction.getIdDelBaze() != null){
                    rezimTv.setText("Offline brisanje");
                    deleteBtn.setText("Undo");
                }
                else if (transaction.getType() != null) {
                    rezimTv.setText("Offline izmjena");
                }

                else {
                    rezimTv.setText("Offline dodavanje");
                }
            }

            ArrayList<String> types = new ArrayList<>();
            types.addAll(getPresenter().getTypes());
            types.remove("All");
            adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, types);
            typeSpinner.setAdapter(adapter);
            if (transaction.getType() != null) {
                typeSpinner.setSelection(types.indexOf(transaction.getType().toString()));
            } else {
                typeSpinner.setSelection(1);
            }

            postavkaVidljivosti();

            typeSpinner.setOnItemSelectedListener(spinnerLIstener);
            deleteBtn.setOnClickListener(deleteBtnListener);
            titleEt.addTextChangedListener(titleListener);
            amountEt.addTextChangedListener(amountListener);
            descriptionEt.addTextChangedListener(descriptionListener);
            intervalEt.addTextChangedListener(intervalListener);
            saveBtn.setOnClickListener(saveBtnListener);


            dateTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        DatePickerDialog dialog = new DatePickerDialog(
                                getActivity(),
                                android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth,
                                dateSetListener,
                                year, month, day);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
                        dialog.show();
                    }
                }
            });


            dateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    month = month + 1;
                    String date = dayOfMonth + "/" + month + "/" + year;
                    dateTv.setText(date);
                    pomocnaDatum();
                    if(broj){
                        if (transaction.getDate().compareTo(datum1) != 0)
                            dateTv.setBackgroundColor(Color.GREEN);
                        else dateTv.setBackgroundColor(Color.TRANSPARENT);
                    }
                }
            };

            endDateTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        DatePickerDialog dialog = new DatePickerDialog(
                                getActivity(),
                                android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth,
                                endDateSetListener,
                                year, month, day);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
                        dialog.show();
                    }
                }
            });


            endDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    month = month + 1;
                    String date = dayOfMonth + "/" + month + "/" + year;
                    endDateTv.setText(date);
                    pomocnaDatum();
                    if(broj) {
                        if (transaction.getEndDate() == null || transaction.getEndDate().compareTo(datum2) != 0)
                            endDateTv.setBackgroundColor(Color.GREEN);
                        else endDateTv.setBackgroundColor(Color.TRANSPARENT);
                    }
                }
            };


        }


        onDeleteClick = (OnDeleteClick) getActivity();
        onSaveClick = (OnSaveClick) getActivity();
        return view;
    }

    private TextWatcher descriptionListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(broj) descriptionEt.setBackgroundColor(Color.GREEN);
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(broj && transaction.getItemDescription() != null &&
                    transaction.getItemDescription().equals(descriptionEt.getText().toString()))
                descriptionEt.setBackgroundColor(Color.TRANSPARENT);
        }
    };

    private TextWatcher intervalListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(broj) intervalEt.setBackgroundColor(Color.GREEN);
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(broj && transaction.getTransactionInterval() != null && !intervalEt.getText().toString().isEmpty() &&
                    transaction.getTransactionInterval().compareTo(Integer.parseInt(intervalEt.getText().toString()))== 0)
                intervalEt.setBackgroundColor(Color.TRANSPARENT);
        }
    };


    private TextWatcher amountListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (broj) amountEt.setBackgroundColor(Color.GREEN);
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (broj && !amountEt.getText().toString().equals(".00") && !amountEt.getText().toString().isEmpty() && transaction.getAmount().compareTo(Double.parseDouble(amountEt.getText().toString())) == 0) {
                amountEt.setBackgroundColor(Color.TRANSPARENT);
            }
        }
    };

    private TextWatcher titleListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (broj) titleEt.setBackgroundColor(Color.GREEN);
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (broj && transaction.getTitle().equals(titleEt.getText().toString())) {
                titleEt.setBackgroundColor(Color.TRANSPARENT);
            }
        }
    };

    private AdapterView.OnItemSelectedListener spinnerLIstener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            postavkaVidljivosti();
            if (broj) {
                if (!transaction.getType().toString().equals(typeSpinner.getSelectedItem().toString()))
                    typeSpinner.setBackgroundColor(Color.GREEN);
                else typeSpinner.setBackgroundColor(Color.TRANSPARENT);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            postavkaVidljivosti();
        }
    };

    private AdapterView.OnClickListener deleteBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Brisanje transakcije");
            alert.setMessage("Da li ste sigurni da zelite nastaviti?");
            alert.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getPresenter().delete(transaction.getId());
                    onDeleteClick.onDeleteClicked();
                }
            });
            alert.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.create().show();
        }
    };

    private AdapterView.OnClickListener saveBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String ts = typeSpinner.getSelectedItem().toString().replaceAll("\\s+", "").toUpperCase();
            pomocnaDatum();
            Double amount = null;
            Integer interval = null;
            String title = titleEt.getText().toString().trim();
            String desc = descriptionEt.getText().toString().trim();
            if (title.length() < 4 || title.length() > 14) {
                Toast.makeText(getActivity(), "Naziv transakcije treba ima minimalno 4, a maksimalno 14 znakova", Toast.LENGTH_LONG).show();
            } else if (amountEt.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Unesite iznos", Toast.LENGTH_LONG).show();
            } else if (datum1 == null) {
                Toast.makeText(getActivity(), "Niste unijeli datum", Toast.LENGTH_LONG).show();
            } else if (ts.equals("INDIVIDUALPAYMENT")) {
                datum2 = null;
                if (desc.equals(""))
                    Toast.makeText(getActivity(), "Unesite opis", Toast.LENGTH_LONG).show();
                else {
                    amount = Double.parseDouble(amountEt.getText().toString());
                    savePomocna(amount, title, ts, desc, interval);
                }
            } else if (ts.equals("REGULARPAYMENT")) {
                if (datum2 != null && datum2.before(datum1)) {
                    Toast.makeText(getActivity(), "EndDate ne moze biti prije Date", Toast.LENGTH_LONG).show();
                } else if (intervalEt.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Niste unijeli interval", Toast.LENGTH_LONG).show();
                } else if (desc.equals("")) {
                    Toast.makeText(getActivity(), "Niste unijeli opis", Toast.LENGTH_LONG).show();
                } else if (datum2 == null) {
                    Toast.makeText(getActivity(), "Niste unijeli endDate", Toast.LENGTH_LONG).show();
                } else {
                    amount = Double.parseDouble(amountEt.getText().toString());
                    interval = Integer.parseInt(intervalEt.getText().toString());
                    savePomocna(amount, title, ts, desc, interval);
                }
            } else if (ts.equals("PURCHASE")) {
                datum2 = null;
                if (desc.equals("")) {
                    Toast.makeText(getActivity(), "Niste unijeli opis", Toast.LENGTH_LONG).show();
                } else {
                    amount = Double.parseDouble(amountEt.getText().toString());
                    savePomocna(amount, title, ts, desc, interval);
                }
            } else if (ts.equals("INDIVIDUALINCOME")) {
                datum2 = null;
                amount = Double.parseDouble(amountEt.getText().toString());
                savePomocna(amount, title, ts, null, interval);
            } else {
                if (datum2 != null && datum2.before(datum1)) {
                    Toast.makeText(getActivity(), "EndDate ne moze biti prije Date", Toast.LENGTH_LONG).show();
                } else if (intervalEt.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Niste unijeli interval", Toast.LENGTH_LONG).show();
                } else if (datum2 == null) {
                    Toast.makeText(getActivity(), "Niste unijeli endDate", Toast.LENGTH_LONG).show();
                } else {
                    interval = Integer.parseInt(intervalEt.getText().toString());
                    amount = Double.parseDouble(amountEt.getText().toString());
                    savePomocna(amount, title, ts, null, interval);
                }
            }
        }
    };

    public void savePomocna(final Double amount, final String title, final String ts, final String desc, final Integer inter){
        Double pomocna = amount;
        if(ts.equals("INDIVIDUALPAYMENT") || ts.equals("PURCHASE")){
            pomocna *= -1;
            if((getPresenter().getTotalLimit()*(-1)) >= (getPresenter().getTotalAmount() + pomocna)){

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Potvrda");
                alert.setMessage("Ovom izmjenom prelazite ukupni limit. Zelite li nastaviti?");
                alert.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
                        if(!broj) {
                            getPresenter().addTransaction();
                            broj = true;
                        }
                        vratiBoje();
                        onSaveClick.onSaveClicked();
                    }
                });
                alert.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create().show();
            }
            else if(getPresenter().getMonthLimit()*-1 >= (getPresenter().getMonthAmount(datum1, 0) + pomocna)){

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Potvrda");
                alert.setMessage("Ovom izmjenom prelazite mjesecni limit. Zelite li nastaviti?");
                alert.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
                        if(!broj) {
                            getPresenter().addTransaction();
                            broj = true;
                        }
                        vratiBoje();
                        onSaveClick.onSaveClicked();
                    }
                });
                alert.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create().show();
            } else {
                getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
                if(!broj) {
                    getPresenter().addTransaction();
                    broj = true;
                }
                vratiBoje();
                onSaveClick.onSaveClicked();
            }
        }
        else if(ts.equals("REGULARPAYMENT")){
            Double pomocnaRegular = pomocnaF1(amount, inter);
            Double pomocna2 = pomocnaF2(amount, inter);

            if((getPresenter().getTotalLimit()*(-1)) >= (getPresenter().getTotalAmount() + pomocnaRegular)){

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Potvrda");
                alert.setMessage("Ovom izmjenom prelazite ukupni limit. Zelite li nastaviti?");
                alert.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
                        if(!broj) {
                            getPresenter().addTransaction();
                            broj = true;
                        }
                        vratiBoje();
                        onSaveClick.onSaveClicked();
                    }
                });
                alert.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create() .show();
            }
            else if(getPresenter().getMonthLimit()*-1 >= (getPresenter().getMonthAmount(datum1, 0) + pomocna2)){

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Potvrda");
                alert.setMessage("Ovom izmjenom prelazite mjesecni limit. Zelite li nastaviti?");
                alert.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
                        if(!broj) {
                            getPresenter().addTransaction();
                            broj = true;
                        }
                        vratiBoje();
                        onSaveClick.onSaveClicked();
                    }
                });
                alert.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create() .show();
            } else {
                getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
                if(!broj) {
                    getPresenter().addTransaction();
                    broj = true;
                }
                vratiBoje();
                onSaveClick.onSaveClicked();
            }
        } else {
            getPresenter().update(broj, datum1, amount, title, TransactionType.valueOf(ts), desc, inter, datum2);
            if(!broj) {
                getPresenter().addTransaction();
                broj = true;
            }
            vratiBoje();
            onSaveClick.onSaveClicked();
        }

    }

    private void vratiBoje(){
        titleEt.setBackgroundColor(Color.TRANSPARENT);
        typeSpinner.setBackgroundColor(Color.TRANSPARENT);
        amountEt.setBackgroundColor(Color.TRANSPARENT);
        dateTv.setBackgroundColor(Color.TRANSPARENT);
        intervalEt.setBackgroundColor(Color.TRANSPARENT);
        descriptionEt.setBackgroundColor(Color.TRANSPARENT);
        endDateTv.setBackgroundColor(Color.TRANSPARENT);
    }

    private Double pomocnaF2(Double amount, Integer inter) {
        Double pomocnaRegular;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datum1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        Date lastDayOfMonth = calendar.getTime();
        Date firstDayOfMonth = new Date(datum1.getYear(), datum1.getMonth(), 1);

        Date dat1 = null;
        Date dat2 = null;
        if(firstDayOfMonth.after(datum1)){
            dat1 = firstDayOfMonth;
        } else{
            dat1 = datum1;
        }
        if(lastDayOfMonth.before(datum2)){
            dat2 = lastDayOfMonth;
        } else {
            dat2 = datum2;
        }
        long brDana = dat2.getTime() - dat1.getTime();
        brDana = TimeUnit.DAYS.convert(brDana, TimeUnit.MILLISECONDS);
        brDana++;
        brDana = brDana / inter;
        pomocnaRegular = brDana * amount;
        pomocnaRegular*=-1;
        return pomocnaRegular;
    }


    Double pomocnaF1(Double amount, Integer inter){
        Double pomocnaRegular = 0.0;
        long brDana = datum2.getTime() - datum1.getTime();
        brDana = TimeUnit.DAYS.convert(brDana, TimeUnit.MILLISECONDS);
        brDana++;
        brDana = brDana / inter;
        pomocnaRegular = brDana * amount;
        pomocnaRegular*=-1;
        return pomocnaRegular;
    }


    public void postavkaVidljivosti() {
        if (typeSpinner.getSelectedItem().toString().substring(0, 7).equals("Regular")) {
            if (typeSpinner.getSelectedItem().toString().equals("Regular income")) {
                descriptionEt.setVisibility(View.GONE);
            } else {
                descriptionEt.setVisibility(View.VISIBLE);
            }
            intervalEt.setVisibility(View.VISIBLE);
            endDateTv.setVisibility(View.VISIBLE);
        } else if (typeSpinner.getSelectedItem().toString().equals("Purchase")) {
            descriptionEt.setVisibility(View.VISIBLE);
            intervalEt.setVisibility(View.GONE);
            endDateTv.setVisibility(View.GONE);
        } else {
            intervalEt.setVisibility(View.GONE);
            endDateTv.setVisibility(View.GONE);
            if (typeSpinner.getSelectedItem().toString().equals("Individual payment")) {
                descriptionEt.setVisibility(View.VISIBLE);
            } else {
                descriptionEt.setVisibility(View.GONE);
            }
        }
    }

    private void pomocnaDatum() {
        try {
            datum1 = format.parse(dateTv.getText().toString());
            if(!endDateTv.getText().toString().isEmpty()) {
                datum2 = format.parse(endDateTv.getText().toString());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
