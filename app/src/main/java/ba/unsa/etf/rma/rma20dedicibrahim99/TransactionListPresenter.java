package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.ArrayAdapter;

import com.github.mikephil.charting.components.AxisBase;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TransactionListPresenter implements ITransactionListPresenter, PostAccount.OnPostAccountDone, TransactionListInteractor.OnTransactionsSearchDone, AccountInteractor.OnAccountSearchDone {

    private ITransactionListView view;
    private Context context;
    ArrayList<Transaction> transactions;
    Account account;
    private ITransactionListInteractor interactor;
    private IAccountInteractor iAccountInteractor;

    public TransactionListPresenter(ITransactionListView view, Context context) {
        this.context = context;
        this.view = view;
        transactions = new ArrayList<>();
        account = new Account();
        interactor = new TransactionListInteractor();
        iAccountInteractor = new AccountInteractor();
    }

    @Override
    public void refreshTransaction() {
        if(interactor.getTransaction() != null) {
            view.setTransaction(interactor.getTransaction());
        } else {
            try {
                interactor.setTransactions(vratiIzBaze());
                view.setTransaction(vratiIzBaze());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        view.setTransactionType(interactor.getTransactionType());
        view.setSortType(interactor.getSortType());
        view.notifyTransactionListDataSetChanged();
    }

    private ArrayList<Transaction> vratiIzBaze() throws ParseException {
        String URL = "content://ba.unsa.etf.rma.rma20dedicibrahim99transactions.CP/transactions";
        Uri transactions = Uri.parse(URL);
        ArrayList<Transaction> transakcijeBaze = new ArrayList();
        Cursor c = context.getContentResolver().query(transactions, null, null, null, "_id");
        if(c.moveToFirst()){
            do{
                System.out.println("datum iz baze: " + c.getString(c.getColumnIndex(TransactionsProvider.DATE)));
                transakcijeBaze.add(new Transaction(
                        1,
                        c.getString(c.getColumnIndex(TransactionsProvider._ID)),
                        c.getString(c.getColumnIndex(TransactionsProvider.ID)),
                        c.getString(c.getColumnIndex(TransactionsProvider.DATE)),
                        c.getString(c.getColumnIndex(TransactionsProvider.AMOUNT)),
                        c.getString(c.getColumnIndex(TransactionsProvider.TITLE)),
                        c.getString(c.getColumnIndex(TransactionsProvider.TYPEID)),
                        c.getString(c.getColumnIndex(TransactionsProvider.ITEMDESCRIPTION)),
                        c.getString(c.getColumnIndex(TransactionsProvider.INTERVAL)),
                        c.getString(c.getColumnIndex(TransactionsProvider.ENDDATE))));
            } while (c.moveToNext());
        }
        String URL1 = "content://ba.unsa.etf.rma.rma20dedicibrahim99transactionsdel.CP/transactions";
        Uri transactionsdel = Uri.parse(URL1);
        Cursor c1 = context.getContentResolver().query(transactionsdel, null, null, null, "_id");
        if(c1.moveToFirst()){
            do{
                transakcijeBaze.add(new Transaction(
                        2,
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider._ID)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.ID)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.DATE)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.AMOUNT)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.TITLE)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.TYPEID)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.ITEMDESCRIPTION)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.INTERVAL)),
                        c1.getString(c1.getColumnIndex(TransactionsDeleteProvider.ENDDATE))));
            } while (c1.moveToNext());
        }
        return transakcijeBaze;
    }

    @Override
    public void refreshList() {
        view.setTransaction(interactor.getTransaction());
        view.notifyTransactionListDataSetChanged();
    }


    @Override
    public void backTransaction() {
        view.setTransaction(interactor.getTransaction());
    }

    @Override
    public void getGlobalAmount() {
        Double rez = 0.0;
        if(interactor.getTransaction() != null) {
            for (Transaction t : interactor.getTransaction()) {
                Double amount = t.getAmount();
                if (!t.getType().toString().contains("income")) {
                    amount *= -1;
                }
                if (t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income")) {
                    int brDana = 0;
                    Calendar c = Calendar.getInstance();
                    Date pocetak = new Date(String.valueOf(t.getDate()));
                    while (pocetak.before(t.getEndDate())) {
                        c.setTime(pocetak);
                        c.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
                        pocetak = c.getTime();
                        brDana++;
                    }
                    rez = rez + (brDana * amount);
                } else {
                    rez += amount;
                }
            }
        }
        iAccountInteractor.getAccount().setBudget(rez);
        new PostAccount((PostAccount.OnPostAccountDone)this).execute(rez.toString(), "budzet");
        view.setGlobalAmount(rez);
    }

    @Override
    public void getTotalLimit() {
        if(iAccountInteractor.getAccount().getTotalLimit() != null) {
            view.setLimit(iAccountInteractor.getAccount().getTotalLimit());
        } else {
            iAccountInteractor.getAccount().setMonthLimit(700.0);
            iAccountInteractor.getAccount().setTotalLimit(1000.0);
            view.setLimit(0.0);
        }
    }

    @Override
    public void searchTransaction() {
        new TransactionListInteractor((TransactionListInteractor.OnTransactionsSearchDone)this).execute();
    }

    @Override
    public void searchAccount() {
        new AccountInteractor((AccountInteractor.OnAccountSearchDone)this).execute();
    }


    @Override
    public void onDone(ArrayList<Transaction> results) {
        interactor.setTransactions(results);
        view.setTransactions();
    }

    @Override
    public void onDone(Account results) {
        iAccountInteractor.setAccount(results);
        view.setLimit(results.getTotalLimit());
        //view.setGlobalAmount(results.getBudget());
    }

    @Override
    public void onDone() {

    }
}
