package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AccountInteractor extends AsyncTask<Void, Integer, Void> implements IAccountInteractor {
    private String id = "9a575e28-e19d-442b-b946-5ebad091e114";
    private String root = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private Account account;
    private OnAccountSearchDone caller;

    public AccountInteractor(OnAccountSearchDone caller) {
        this.caller = caller;
    }

    public AccountInteractor() {

    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    public interface OnAccountSearchDone {
        public void onDone(Account results);
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(account);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        String url1 = root + "/account/" + id;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);
                Integer id = jo.getInt("id");
                Double budget = jo.getDouble("budget");
                Double total = jo.getDouble("totalLimit");
                Double month = jo.getDouble("monthLimit");
                account = new Account(id, budget, total, month);
            } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public Account getAccount() {
        return AccountsModel.account;
    }

    @Override
    public void setAccount(Account results) {
        AccountsModel.account = results;
    }

    @Override
    public void updateMonthLimit(Double limit) {
        AccountsModel.account.setMonthLimit(limit);
    }

    @Override
    public void updateTotalLimit(Double limit) {
        AccountsModel.account.setTotalLimit(limit);
    }


}