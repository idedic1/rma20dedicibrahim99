package ba.unsa.etf.rma.rma20dedicibrahim99;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.app.FragmentBreadCrumbs;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick,
        TransactionDetailFragment.OnDeleteClick, TransactionDetailFragment.OnSaveClick, OnItemTouch{

    private boolean twoPaneMode;
    private ConnectivityBroadcastReceiver receiver = new ConnectivityBroadcastReceiver();
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout detail = findViewById(R.id.transaction_detail);
        if(detail != null) {
            twoPaneMode = true;
            TransactionDetailFragment detailFragment = (TransactionDetailFragment) fragmentManager.findFragmentById(R.id.transaction_detail);
            if (detailFragment == null) {
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().replace(R.id.transaction_detail, detailFragment).commit();
            }
        }else {
            twoPaneMode = false;
        }
        Fragment listFragment = fragmentManager.findFragmentById(R.id.transaction_list);
        if(listFragment == null){
            listFragment = new TransactionListFragment();
            fragmentManager.beginTransaction().replace(R.id.transaction_list, listFragment).commit();
        } else {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

    }

    public Transaction getTransakcija(){
        TransactionDetailFragment fragment = (TransactionDetailFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_detail);
        return fragment.getPresenter().getTransaction();
    }


    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);

    }

    @Override
    public void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    public void onItemClicked(Transaction transaction, Boolean oznaka) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("transaction", transaction);
        arguments.putBoolean("oznaka", oznaka);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if(twoPaneMode){
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail, detailFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, detailFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onDeleteClicked() {
        if(twoPaneMode){
            TransactionListFragment fragment = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            fragment.postaviLandscape(1);
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onSaveClicked() {
        if(twoPaneMode){
            TransactionListFragment fragment = (TransactionListFragment) getSupportFragmentManager().findFragmentById(R.id.transaction_list);
            fragment.postaviLandscape(0);
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }



    @Override
    public void onLeftTouch(int no) {
        if(!twoPaneMode){
            switch (no){
                case 0:
                    TransactionListFragment fragmentList = new TransactionListFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, fragmentList).addToBackStack(null).commit();
                    break;
                case 1:
                    BudgetFragment fragmentBudget = new BudgetFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, fragmentBudget).addToBackStack(null).commit();
                    break;
                case 2:
                    GraphsFragment graphsFragment = new GraphsFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, graphsFragment).addToBackStack(null).commit();
                default:
                    break;
            }
        }
    }

    @Override
    public void onRightTocuh(int no) {
        if(!twoPaneMode){
            switch (no){
                case 0:
                    TransactionListFragment fragmentList = new TransactionListFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, fragmentList).addToBackStack(null).commit();
                    break;
                case 1:
                    BudgetFragment fragmentBudget = new BudgetFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, fragmentBudget).addToBackStack(null).commit();
                    break;
                case 2:
                    GraphsFragment graphsFragment = new GraphsFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, graphsFragment).addToBackStack(null).commit();
                default:
                    break;
            }
        }
    }
}
