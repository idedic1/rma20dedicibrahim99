package ba.unsa.etf.rma.rma20dedicibrahim99;

public interface IBudgetFragment {
    void setAccount(Account account);
    void notifyDataSetChanged();
}
