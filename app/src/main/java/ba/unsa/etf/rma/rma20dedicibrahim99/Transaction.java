package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Transaction implements Parcelable {
    private String idBaze = null;



    public String getIdDelBaze() {
        return idDelBaze;
    }

    public void setIdDelBaze(String idDelBaze) {
        this.idDelBaze = idDelBaze;
    }

    private String idDelBaze = null;
    private Integer id;
    private Date date;
    private Double amount;
    private String title;
    private TransactionType type;
    private String itemDescription;
    private Integer transactionInterval;
    private Date endDate;

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);


    public Transaction(Date date, double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate) {
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
    }

    private TransactionType vratiTip(String s){
        TransactionType tip = null;
        switch (s){
            case "5":
                tip = TransactionType.INDIVIDUALPAYMENT;
                break;
            case "1":
                tip = TransactionType.REGULARPAYMENT;
                break;
            case "3":
                tip = TransactionType.PURCHASE;
                break;
            case "4":
                tip = TransactionType.INDIVIDUALINCOME;
                break;
            case "2":
                tip = TransactionType.REGULARINCOME;
                break;
        }
        return tip;
    }

    public Transaction(int i, String _id, String id, String string, String amount, String string1, String string2, String string3, String string4, String string5) throws ParseException {
        if(i==1){
            this.idBaze = _id;
        } else {
            this.idDelBaze = _id;
        }
        this.id = Integer.parseInt(id);
        if(i==1) {
            this.date = postajDatum(formatter.parse(string));
        } else {
            this.date = formatter.parse(string);
        }
        this.amount = Double.parseDouble(amount);
        this.title = string1;
        this.type = vratiTip(string2);
        this.itemDescription = string3;
        if(string2.equals("1") || string2.equals("2")){
            this.transactionInterval = Integer.parseInt(string4);
            if(i==1) {
                this.endDate = postajDatum(formatter.parse(string5));
            } else {
                this.endDate = formatter.parse(string5);
            }
        } else {
            this.transactionInterval = null;
            this.endDate = null;
        }
    }

    private Date postajDatum(Date datum){
        Calendar cal = Calendar.getInstance();
        cal.setTime(datum);
        cal.add(Calendar.DATE, -1);
        Date date1 = cal.getTime();
        return date1;
    }

    public Transaction(Integer id, String date, double amount, String title, Integer type, String itemDescription, String transactionInterval, String endDate) throws ParseException {
        this.id = id;
        this.date = format.parse(date);
        this.amount = amount;
        this.title = title;
        this.type = odrediType(type);
        this.itemDescription = itemDescription;
        if(type == 1 || type == 2){
            this.transactionInterval = Integer.valueOf(transactionInterval );
            this.endDate = format.parse(endDate);
        } else {
            this.transactionInterval = null;
            this.endDate = null;
        }
    }

    public String getIdBaze() {
        return idBaze;
    }

    public void setIdBaze(String idBaze) {
        this.idBaze = idBaze;
    }

    private TransactionType odrediType(Integer type) {
        if(type == 1) return TransactionType.REGULARPAYMENT;
        if(type == 2) return TransactionType.REGULARINCOME;
        if(type == 3) return TransactionType.PURCHASE;
        if(type == 4) return TransactionType.INDIVIDUALINCOME;
        if(type == 5) return TransactionType.INDIVIDUALPAYMENT;
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        //dest.writeSerializable(id);
        dest.writeSerializable(date);
        dest.writeSerializable(amount);
        dest.writeString(title);
        dest.writeSerializable(type);
        dest.writeString(itemDescription);
        dest.writeSerializable(transactionInterval);
        dest.writeSerializable(endDate);
    }

    public Transaction() {

    }

    protected Transaction(Parcel in) {
        //id = in.readInt();
        date = (Date) in.readSerializable();
        amount = in.readDouble();
        title = in.readString();
        type = (TransactionType) in.readSerializable();
        itemDescription = in.readString();
        transactionInterval = in.readInt();
        endDate = (Date) in.readSerializable();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Integer getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(Integer transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }


}