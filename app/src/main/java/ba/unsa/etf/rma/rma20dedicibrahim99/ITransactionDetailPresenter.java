package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

public interface ITransactionDetailPresenter {
    void create(Date date, Double amount, String title, TransactionType type, String itemDescription, int transactionInterval, Date endDate);

    void update(boolean oznaka, Date date, Double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate);

    Transaction getTransaction();

    void setTransaction(Parcelable transaction);

    ArrayList<String> getTypes();

    void delete(int id);



    void addTransaction();

    Double getMonthAmount(Date date, int oznaka);
    Double getTotalAmount();

    Double getMonthLimit();
    Double getTotalLimit();
}

