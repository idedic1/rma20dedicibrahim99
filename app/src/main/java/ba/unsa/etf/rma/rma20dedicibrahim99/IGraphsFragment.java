package ba.unsa.etf.rma.rma20dedicibrahim99;

import java.util.ArrayList;

public interface IGraphsFragment {
    void setTransactions();
    void notifyDataSetChanged();
}
