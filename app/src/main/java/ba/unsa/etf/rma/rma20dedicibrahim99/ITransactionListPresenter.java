package ba.unsa.etf.rma.rma20dedicibrahim99;

public interface ITransactionListPresenter {
    void refreshTransaction();
    void refreshList();
    void backTransaction();
    void getGlobalAmount();
    void getTotalLimit();
    void searchTransaction();
    void searchAccount();
}