package ba.unsa.etf.rma.rma20dedicibrahim99;

import java.util.ArrayList;
import java.util.Date;

public interface IGraphsPresenter {
    public void searchTransactions();
    Double getMonthAmount(Date date, int oznaka);
    ArrayList<Double> getMjesecno(int oznaka);
    ArrayList<Double> getDnevno(int oznaka);
    ArrayList<Double> getSedmicno(int oznaka);
}
