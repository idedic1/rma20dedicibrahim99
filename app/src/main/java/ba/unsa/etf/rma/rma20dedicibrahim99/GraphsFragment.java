package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class GraphsFragment extends Fragment implements IGraphsFragment {

    private OnItemTouch onItemTouch;
    BarChart ukupnoBar;
    BarChart negativBar;
    BarChart pozitivBar;
    Button mjesecBtn;
    Button sedmicaBtn;
    Button danBtn;
    ArrayList<Double> listaUkupnoMjesec;
    ArrayList<Double> listaPozitivMjesec;
    ArrayList<Double> listaNegativMjesec;
    ArrayList<Double> listaUkupnoDan;
    ArrayList<Double> listaPozitivDan;
    ArrayList<Double> listaNegativDan;
    ArrayList<Double> listaUkupnoSedmica;
    ArrayList<Double> listaPozitivSedmica;
    ArrayList<Double> listaNegativSedmica;
    ArrayList<Transaction> transactions = new ArrayList<>();

    private IGraphsPresenter presenter;

    public IGraphsPresenter getPresenter() {
        if (presenter == null) {
            presenter = new GraphsPresenter(this, getActivity()) {
            };
        }
        return presenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getPresenter().searchTransactions();
        View view = inflater.inflate(R.layout.fragment_graphs, container, false);
        onItemTouch = (OnItemTouch) getActivity();
        ukupnoBar = view.findViewById(R.id.barUkupno);
        negativBar = view.findViewById(R.id.barNegativ);
        pozitivBar = view.findViewById(R.id.barPozivit);
        mjesecBtn = view.findViewById(R.id.button6);
        sedmicaBtn = view.findViewById(R.id.button7);
        danBtn = view.findViewById(R.id.button8);


        danBtn.setOnClickListener(danBtnListener);
        mjesecBtn.setOnClickListener(mjesecBtnListener);
        sedmicaBtn.setOnClickListener(sedmicaBtnListener);

        view.setOnTouchListener(swipeListener);
        return view;
    }

    private View.OnClickListener danBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listaUkupnoDan = getPresenter().getDnevno(1);
            listaPozitivDan = getPresenter().getDnevno(2);
            listaNegativDan = getPresenter().getDnevno(3);
            setUkupno(getData(listaUkupnoDan));
            setPozitiv(getData(listaPozitivDan));
            setNegativ(getData(listaNegativDan));
        }
    };

    private View.OnClickListener mjesecBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listaUkupnoMjesec = getPresenter().getMjesecno(1);
            listaPozitivMjesec = getPresenter().getMjesecno(2);
            listaNegativMjesec = getPresenter().getMjesecno(3);
            setUkupno(getData(listaUkupnoMjesec));
            setPozitiv(getData(listaPozitivMjesec));
            setNegativ(getData(listaNegativMjesec));
        }
    };

    private View.OnClickListener sedmicaBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listaUkupnoSedmica = getPresenter().getSedmicno(1);
            listaPozitivSedmica = getPresenter().getSedmicno(2);
            listaNegativSedmica = getPresenter().getSedmicno(3);
            setUkupno(getData(listaUkupnoSedmica));
            setPozitiv(getData(listaPozitivSedmica));
            setNegativ(getData(listaNegativSedmica));
        }
    };

    private void setNegativ(ArrayList<BarEntry> lista){
        BarDataSet barDataSet = new BarDataSet(lista, "Potrošnja");
        BarData barData = new BarData();
        barData.addDataSet(barDataSet);
        negativBar.setData(barData);
        negativBar.invalidate();
    }


    private void setPozitiv(ArrayList<BarEntry> lista){
        BarDataSet barDataSet = new BarDataSet(lista, "Zarada");
        BarData barData = new BarData();
        barData.addDataSet(barDataSet);
        pozitivBar.setData(barData);
        pozitivBar.invalidate();
    }


    private void setUkupno(ArrayList<BarEntry> lista){
        BarDataSet barDataSet = new BarDataSet(lista, "Ukupno stanje");
        BarData barData = new BarData();
        barData.addDataSet(barDataSet);
        ukupnoBar.setData(barData);
        ukupnoBar.invalidate();
    }



    private ArrayList<BarEntry> getData(ArrayList<Double> lista){
        ArrayList<BarEntry> data = new ArrayList<>();
        for(int i=0; i<lista.size(); i++){
            int y = (int) Math.round(lista.get(i));
            data.add(new BarEntry(i+1, y));
        }
        return data;
    }

    private OnSwipeTouchListener swipeListener = new OnSwipeTouchListener(getActivity()){
        @Override
        public void onSwipeLeft() {
            onItemTouch.onLeftTouch(0);
        }
        @Override
        public void onSwipeRight() {
            onItemTouch.onRightTocuh(1);
        }
    };

    @Override
    public void setTransactions() {
        listaUkupnoMjesec = getPresenter().getMjesecno(1);
        listaPozitivMjesec = getPresenter().getMjesecno(2);
        listaNegativMjesec = getPresenter().getMjesecno(3);
        setUkupno(getData(listaUkupnoMjesec));
        setPozitiv(getData(listaPozitivMjesec));
        setNegativ(getData(listaNegativMjesec));
    }

    @Override
    public void notifyDataSetChanged() {

    }
}
