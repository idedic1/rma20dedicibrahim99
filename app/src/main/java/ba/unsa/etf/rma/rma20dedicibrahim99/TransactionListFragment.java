package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class TransactionListFragment extends Fragment implements ITransactionListView {

    private TextView amountTv;
    private TextView limitTv;
    private Spinner filterSpinner;
    private ImageButton leftBtn;
    private ImageButton rightBtn;
    private TextView dateTv;
    private Spinner sortSpinner;
    private ListView transactionLv;
    private Button addBtn;


    String pattern = "MMMM, yyyy";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    Calendar calendar = Calendar.getInstance();


    private ITransactionListPresenter transactionListPresenter;
    private TransactionListAdapter transactionListAdapter;
    private TransactionTypeSpinnerAdapter transactionTypeSpinnerAdapter;
    private ArrayAdapter sortAdapter;

    public TransactionListAdapter getTransactionListAdapter(){
        return transactionListAdapter;
    }


    public ITransactionListPresenter getTransactionListPresenter() {
        if (transactionListPresenter == null) {
            transactionListPresenter = new TransactionListPresenter(this, getActivity());
        }
        return transactionListPresenter;
    }

    private OnItemClick onItemClick;
    public interface OnItemClick{
        public void onItemClicked(Transaction transaction, Boolean oznaka);
    }

    private OnItemTouch onItemTouch;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transaction_list, container, false);

        amountTv = (TextView) view.findViewById(R.id.textView1);
        limitTv = (TextView) view.findViewById(R.id.textView2);
        filterSpinner = (Spinner) view.findViewById(R.id.spinner1);
        leftBtn = (ImageButton) view.findViewById(R.id.button1);
        rightBtn = (ImageButton) view.findViewById(R.id.button2);
        dateTv = (TextView) view.findViewById(R.id.textView3);
        sortSpinner = (Spinner) view.findViewById(R.id.spinner2);
         addBtn = (Button) view.findViewById(R.id.button3);



        transactionTypeSpinnerAdapter = new TransactionTypeSpinnerAdapter(getActivity(), new ArrayList<String>());
        filterSpinner.setAdapter(transactionTypeSpinnerAdapter);

        String date = simpleDateFormat.format(calendar.getTime());
        dateTv.setText(date);

        sortAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
        sortSpinner.setAdapter(sortAdapter);


        transactionListAdapter = new TransactionListAdapter(getActivity(), R.layout.list_element, new ArrayList<Transaction>());
        transactionLv = (ListView) view.findViewById(R.id.listView1);




        filterSpinner.setOnItemSelectedListener(spinnerFilterListener);
        sortSpinner.setOnItemSelectedListener(spinnerItemClickListener);

        transactionLv.setAdapter(transactionListAdapter);
        transactionLv.setOnItemClickListener(listItemClickListener);
        addBtn.setOnClickListener(addBtnClickListener);

        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            getTransactionListPresenter().searchTransaction();
            getTransactionListPresenter().searchAccount();
        } else {
            getTransactionListPresenter().refreshTransaction();
            getTransactionListPresenter().getGlobalAmount();
            getTransactionListPresenter().getTotalLimit();
            leftBtn.setOnClickListener(leftBtnClickListener);
            rightBtn.setOnClickListener(rightBtnClickListener);
        }


        onItemClick = (OnItemClick) getActivity();
        onItemTouch = (OnItemTouch) getActivity();


        transactionLv.setOnTouchListener(swipeListener);
        view.setOnTouchListener(swipeListener);
        return view;
    }

    public void postaviLandscape(int i){
        if(i==1) {
            getTransactionListAdapter().setSelectedTransaction(null);
        }
        getTransactionListPresenter().refreshList();
        getTransactionListAdapter().filterTransaction(filterSpinner.getSelectedItem().toString(), calendar);
        sortPomocna();
        getTransactionListPresenter().getGlobalAmount();
    }

    private OnSwipeTouchListener swipeListener = new OnSwipeTouchListener(getActivity()){
        @Override
        public void onSwipeLeft() {
            onItemTouch.onLeftTouch(1);
        }
        @Override
        public void onSwipeRight() {
            onItemTouch.onRightTocuh(2);
        }
    };

    private AdapterView.OnClickListener addBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Transaction transaction = new Transaction();
            onItemClick.onItemClicked(transaction, false);
        }
    };

    private AdapterView.OnClickListener leftBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            calendar.add(Calendar.MONTH, -1);
            String date = simpleDateFormat.format(calendar.getTime());
            dateTv.setText(date);
            transactionListAdapter.filterTransaction(filterSpinner.getSelectedItem().toString(), calendar);
            sortPomocna();
        }
    };

    private AdapterView.OnClickListener rightBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            calendar.add(Calendar.MONTH, 1);
            String date = simpleDateFormat.format(calendar.getTime());
            dateTv.setText(date);
            transactionListAdapter.filterTransaction(filterSpinner.getSelectedItem().toString(), calendar);
            sortPomocna();
        }
    };


    private static Transaction clickedItem;
    private static int save = -1;


    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = transactionListAdapter.getTransaction(position);
            if(transactionListAdapter.getSelectedTransaction() == transaction){
                parent.getChildAt(position).setBackgroundColor(Color.TRANSPARENT);
                Transaction transaction1 = new Transaction();
                onItemClick.onItemClicked(transaction1, false);
                transactionListAdapter.setSelectedTransaction(null);
                transactionListAdapter.notifyDataSetChanged();
            } else {
                onItemClick.onItemClicked(transaction, true);
                transactionListAdapter.setSelectedTransaction(transaction);
                transactionListAdapter.notifyDataSetChanged();
            }
        }
    };


    private AdapterView.OnItemSelectedListener spinnerFilterListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            transactionListAdapter.filterTransaction(filterSpinner.getSelectedItem().toString(), calendar);
            sortPomocna();

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            transactionListAdapter.filterTransaction(filterSpinner.getSelectedItem().toString(), calendar);
            sortPomocna();
        }
    };

    private void sortPomocna(){
        switch (sortSpinner.getSelectedItemPosition()) {
            case 0:
                transactionListAdapter.sortPricaASC();
                transactionListAdapter.notifyDataSetChanged();
                break;
            case 1:
                transactionListAdapter.sortPricaDSC();
                transactionListAdapter.notifyDataSetChanged();
                break;
            case 2:
                transactionListAdapter.sortTitleASC();
                transactionListAdapter.notifyDataSetChanged();
                break;
            case 3:
                transactionListAdapter.sortTitleDSC();
                transactionListAdapter.notifyDataSetChanged();
                break;
            case 4:
                transactionListAdapter.sortDateASC();
                transactionListAdapter.notifyDataSetChanged();
                break;
            case 5:
                transactionListAdapter.sortDateDSC();
                transactionListAdapter.notifyDataSetChanged();
                break;
            default:
                transactionListAdapter.notifyDataSetChanged();
        }
    }

    private AdapterView.OnItemSelectedListener spinnerItemClickListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            sortPomocna();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            sortPomocna();
        }
    };





    @Override
    public void setTransaction(ArrayList<Transaction> transaction) {
        transactionListAdapter.setTransaction(transaction);
    }

    @Override
    public void setTransactionType(ArrayList<String> transactionType) {
        transactionTypeSpinnerAdapter.setTransactionType(transactionType);
    }

    @Override
    public void setSortType(ArrayList<String> sortType) {
        sortAdapter.addAll(sortType);
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionListAdapter.notifyDataSetChanged();
    }

    @Override
    public void setGlobalAmount(Double rez) {
        amountTv.setText( "Global amount: "+  rez.toString());
    }

    @Override
    public void setLimit(Double limit) {
        if(limit!=null) {
            limitTv.setText("Limit: " + limit.toString());
        } else {
            limitTv.setText("Limit: ");
        }
    }

    @Override
    public void setTransactions() {
        getTransactionListPresenter().refreshTransaction();
        getTransactionListPresenter().getGlobalAmount();
        leftBtn.setOnClickListener(leftBtnClickListener);
        rightBtn.setOnClickListener(rightBtnClickListener);
    }

}
