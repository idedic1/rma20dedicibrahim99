package ba.unsa.etf.rma.rma20dedicibrahim99;

import androidx.annotation.NonNull;

public enum TransactionType {
    INDIVIDUALPAYMENT, REGULARPAYMENT, PURCHASE, INDIVIDUALINCOME, REGULARINCOME;


    @NonNull
    @Override
    public String toString() {
        String tip = "";
        switch (this){
            case INDIVIDUALPAYMENT:
                tip = "Individual payment";
                break;
            case REGULARPAYMENT:
                tip = "Regular payment";
                break;
            case PURCHASE:
                tip = "Purchase";
                break;
            case INDIVIDUALINCOME:
                tip = "Individual income";
                break;
            case REGULARINCOME:
                tip = "Regular income";
                break;
        }
        return tip;
    }

    public String toId(){
        String tip = null;
        switch (this){
            case INDIVIDUALPAYMENT:
                tip = "5";
                break;
            case REGULARPAYMENT:
                tip = "1";
                break;
            case PURCHASE:
                tip = "3";
                break;
            case INDIVIDUALINCOME:
                tip = "4";
                break;
            case REGULARINCOME:
                tip = "2";
                break;
        }
        return tip;
    }
}
