package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.Toast;

import static ba.unsa.etf.rma.rma20dedicibrahim99.AccountProvider.URL;

public class AccountPresenter implements IAccountPresenter, AccountInteractor.OnAccountSearchDone, PostAccount.OnPostAccountDone {


    private Context context;
    private IBudgetFragment view;
    private IAccountInteractor interactor;


    public AccountPresenter(IBudgetFragment view, Context context) {
        this.view = view;
        this.context = context;
        interactor = new AccountInteractor();
    }
    public AccountPresenter(Context context){
        this.context = context;
    }

    public void prenosPodataka(){
        String URL = "content://ba.unsa.etf.rma.rma20dedicibrahim99.CP/accounts";
        Uri accounts = Uri.parse(URL);
        Cursor c = context.getContentResolver().query(accounts, null, null, null, "budget");
        if (c.moveToFirst()) {
            postAccount(c.getString(c.getColumnIndex(AccountProvider.BUDGET)), c.getString(c.getColumnIndex(AccountProvider.TOTALLIMIT)), c.getString(c.getColumnIndex(AccountProvider.MONTHLIMIT)));
            context.getContentResolver().delete(AccountProvider.CONTENT_URI, null, null);
        }
    }

    @Override
    public void dodajUBazu(String s1, String s2, String s3){
        ContentValues values = new ContentValues();
        values.put(AccountProvider._ID, 1);
        values.put(AccountProvider.BUDGET, s1);
        values.put(AccountProvider.TOTALLIMIT,s2);
        values.put(AccountProvider.MONTHLIMIT,s3);
        Uri students = Uri.parse(URL);
        Cursor c = context.getContentResolver().query(students, null, null, null, "budget");
        if (c.moveToFirst()) {
            context.getContentResolver().delete(AccountProvider.CONTENT_URI, null, null);
            context.getContentResolver().insert(AccountProvider.CONTENT_URI, values);
        } else {
            context.getContentResolver().insert(AccountProvider.CONTENT_URI, values);
        }
    }


    @Override
    public void onDone(Account result) {
        view.setAccount(result);
        view.notifyDataSetChanged();
    }

    @Override
    public void searchAccount() {
        new AccountInteractor((AccountInteractor.OnAccountSearchDone)this).execute();
    }

    @Override
    public void postAccount(String... strings) {
        new PostAccount((PostAccount.OnPostAccountDone)this).execute(strings);
    }

    @Override
    public void onDone() {

    }

    @Override
    public Double getTotalLimit() {
        return interactor.getAccount().getTotalLimit();
    }

    @Override
    public Double getMonthLimit() {
        return interactor.getAccount().getMonthLimit();
    }

    @Override
    public Double getBudget() {
        return interactor.getAccount().getBudget();
    }

    @Override
    public void updateMonthLimit(Double limit) {
        interactor.updateMonthLimit(limit);
    }

    @Override
    public void updateTotalLimit(Double limit) {
        interactor.updateTotalLimit(limit);
    }

}
