package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.AsyncTask;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DeleteTransaction extends AsyncTask<Integer, Integer, Void> {
    private String id = "9a575e28-e19d-442b-b946-5ebad091e114";
    private String root = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private Account account;
    private OnDeleteTransactionDone caller;

    public DeleteTransaction(OnDeleteTransactionDone caller) {
        this.caller = caller;
    }

    public interface OnDeleteTransactionDone {
        public void onDone();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        caller.onDone();
    }

    @Override
    protected Void doInBackground(Integer... strings) {
        String url1 = root + "/account/" + id + "/transactions/" + strings[0];
        URL url = null;
        try {
            url = new URL(url1);
        } catch (MalformedURLException exception) {
            exception.printStackTrace();
        }
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestProperty("Content-Type",
                    "application/json; charset=UTF-8");
            httpURLConnection.setRequestMethod("DELETE");
            System.out.println(httpURLConnection.getResponseCode());
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return null;
    }
}
