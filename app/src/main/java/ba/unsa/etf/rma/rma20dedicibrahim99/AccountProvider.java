package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;

public class AccountProvider extends ContentProvider {
    static final String PROVIDER_NAME = "ba.unsa.etf.rma.rma20dedicibrahim99.CP";
    static final String URL = "content://" + PROVIDER_NAME + "/accounts";
    static final Uri CONTENT_URI = Uri.parse(URL);
    static final String _ID = "_id";
    static final String BUDGET = "budget";
    static final String TOTALLIMIT = "totalLimit";
    static final String MONTHLIMIT = "monthLimit";
    private static HashMap<String, String> ACCOUNTS_PROJECTION_MAP;
    static final int ACCOUNTS = 1;
    static final int ACCOUNT_ID = 2;
    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "accounts", ACCOUNTS);
        uriMatcher.addURI(PROVIDER_NAME, "accounts/#", ACCOUNT_ID);
    }

    private SQLiteDatabase db;
    static final String DATABASE_NAME = "Account";
    static final String ACCOUNTS_TABLE_NAME = "accounts";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE = " CREATE TABLE " + ACCOUNTS_TABLE_NAME +
            " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            " budget TEXT NOT NULL, " +
            " totalLimit TEXT NOT NULL, " +
            " monthLimit TEXT NOT NULL);";


    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + ACCOUNTS_TABLE_NAME);
            onCreate(db);
        }
    }


    @Override
    public boolean onCreate() {
        Context context = getContext();
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
        return (db == null) ? false : true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(ACCOUNTS_TABLE_NAME);
        switch (uriMatcher.match(uri)) {
            case ACCOUNTS:
                qb.setProjectionMap(ACCOUNTS_PROJECTION_MAP);
                break;
            case ACCOUNT_ID:
                qb.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        if (sortOrder == null || sortOrder == "") {
            // Sortiranje po imenu
            sortOrder = BUDGET;
        }
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        // Pregled sadrzaja URI-a
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }


    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            // Dohvatanje/preuzimanje svih slogova studenata
            case ACCOUNTS:
                return "vnd.android.cursor.dir/vnd.example.accounts";
                // Dohvatanje/preuzimanje sloga odredjenog studenata
            case ACCOUNT_ID:
                return "vnd.android.cursor.item/vnd.example.accounts";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }


    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        // Dodavanje novog sloga studenta
        long rowID = db.insert(ACCOUNTS_TABLE_NAME, "", values);
        // Ako je slog uspjesno dodan
        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new SQLException("Failed to add a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case ACCOUNTS:
                count = db.delete(ACCOUNTS_TABLE_NAME, selection, selectionArgs);
                break;
            case ACCOUNT_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete(ACCOUNTS_TABLE_NAME, _ID + " = " + id +
                        (!TextUtils.isEmpty(selection) ? " AND (" +
                                selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int count = 0;
        switch (uriMatcher.match(uri)) {
            case ACCOUNTS:
                count = db.update(ACCOUNTS_TABLE_NAME, values, selection, selectionArgs);
                break;
            case ACCOUNT_ID:
                count = db.update(ACCOUNTS_TABLE_NAME, values, _ID + " = " + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
