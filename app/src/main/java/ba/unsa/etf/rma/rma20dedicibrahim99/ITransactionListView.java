package ba.unsa.etf.rma.rma20dedicibrahim99;

import java.util.ArrayList;

public interface ITransactionListView {
    void setTransaction(ArrayList<Transaction> transaction);
    void setTransactionType(ArrayList<String> transactionType);
    void setSortType(ArrayList<String> sortType);
    void notifyTransactionListDataSetChanged();
    void setGlobalAmount(Double rez);
    void setLimit(Double limit);
    void setTransactions();
}

