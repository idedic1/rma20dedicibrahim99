package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class GraphsPresenter implements IGraphsPresenter, TransactionListInteractor.OnTransactionsSearchDone  {
    private Context context;
    private IGraphsFragment view;
    ArrayList<Transaction> transactions;

    public GraphsPresenter(IGraphsFragment view, Context context) {
        this.context = context;
        this.view = view;
        transactions = new ArrayList<>();
    }

    @Override
    public void searchTransactions() {
        new TransactionListInteractor((TransactionListInteractor.OnTransactionsSearchDone)this).execute();
    }

    boolean between(int i, int min, int max){
        return (i>= min && i<=max);
    }

    @Override
    public Double getMonthAmount(Date datum, int oznaka){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datum);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        Date lastDayOfMonth = calendar.getTime();
        Date firstDayOfMonth = new Date(datum.getYear(), datum.getMonth(), 1);
        Date datum1 = null;
        Date datum2 = null;
        Double rez = 0.0;
        for (Transaction t : transactions) {
            if(oznaka == 0) continue;
            if(oznaka == 2 && !t.getType().toString().contains("income")) continue;
            if(oznaka == 3 && t.getType().toString().contains("income")) continue;
            if((t.getDate().getMonth() == datum.getMonth() && t.getDate().getYear() == datum.getYear() )||
                    ((t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income"))
                            && between(datum.getMonth(), t.getDate().getMonth(), t.getEndDate().getMonth()) &&
                            between(datum.getYear(), t.getDate().getYear(), t.getEndDate().getYear()))){
                Double amount = t.getAmount();
                if(!t.getType().toString().contains("income")){
                    amount *= -1;
                }
                if(t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income")){
                    datum1 = firstDayOfMonth;
                    datum2 = lastDayOfMonth;
                    int brDana = 0;
                    Calendar c = Calendar.getInstance();
                    Date pocetak = new Date(String.valueOf(t.getDate()));
                    while(pocetak.before(t.getEndDate())){
                        if((pocetak.after(datum1) && pocetak.before(datum2)) ||
                                (pocetak.getYear()==datum1.getYear() && pocetak.getMonth()==datum1.getMonth() && pocetak.getDay()==datum1.getDay()) ||
                                (pocetak.getYear()==datum2.getYear() && pocetak.getMonth()==datum2.getMonth() && pocetak.getDay()==datum2.getDay())) brDana++;
                        c.setTime(pocetak);
                        c.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
                        pocetak = c.getTime();
                   }
                    rez = rez +(brDana*amount);

                } else {
                    rez += amount;
                }
            }
        }
        return rez;
    }

    @Override
    public ArrayList<Double> getDnevno(int oznaka){
        ArrayList<Double> lista = new ArrayList<Double>();
        for(int i=0; i<366; i++){
            lista.add(0.0);
        }
        Calendar c = Calendar.getInstance();
        Date pocetak = new Date();
        int index = 0;
        for(Transaction t : transactions){
            if(oznaka == 2 && !t.getType().toString().contains("income")) continue;
            if(oznaka == 3 && t.getType().toString().contains("income")) continue;
            Double amount = t.getAmount();
            if(!t.getType().toString().contains("income")){
                amount *= -1;
            }
            c.setTime(t.getDate());
            index = c.get(Calendar.DAY_OF_YEAR);
            if(t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income")){
                pocetak = c.getTime();
                while(pocetak.before(t.getEndDate())){
                    Double p = lista.get(index-1);
                    lista.set(index-1, amount+p);
                    c.setTime(pocetak);
                    c.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
                    index = c.get(Calendar.DAY_OF_YEAR);
                    pocetak = c.getTime();
                }
            } else {
                Double p = lista.get(index-1);
                lista.set(index-1, amount+p);
            }

        }
        if(oznaka == 1){
            for(int i=1; i<lista.size(); i++){
                lista.set(i, lista.get(i-1)+lista.get(i));
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Double> getSedmicno(int oznaka){
        ArrayList<Double> lista = new ArrayList<Double>();
        for(int i=0; i<53; i++){
            lista.add(0.0);
        }
        Calendar c = Calendar.getInstance();
        Date pocetak = new Date();
        int index = 0;
        for(Transaction t : transactions){
            if(oznaka == 2 && !t.getType().toString().contains("income")) continue;
            if(oznaka == 3 && t.getType().toString().contains("income")) continue;
            Double amount = t.getAmount();
            if(!t.getType().toString().contains("income")){
                amount *= -1;
            }
            c.setTime(t.getDate());
            index = c.get(Calendar.WEEK_OF_YEAR);
            if(t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income")){
                pocetak = c.getTime();
                while(pocetak.before(t.getEndDate())){
                    Double p = lista.get(index-1);
                    lista.set(index-1, amount+p);
                    c.setTime(pocetak);
                    c.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
                    index = c.get(Calendar.WEEK_OF_YEAR);
                    pocetak = c.getTime();
                }
            } else {
                Double p = lista.get(index-1);
                lista.set(index-1, amount+p);
            }

        }
        if(oznaka == 1){
            for(int i=1; i<lista.size(); i++){
                lista.set(i, lista.get(i-1)+lista.get(i));
            }
        }
        return lista;
    }

    @Override
    public ArrayList<Double> getMjesecno(int oznaka) {
        ArrayList<Double> lista = new ArrayList<>();
        Calendar c = Calendar.getInstance();
        Date datum = new Date(2020-1900, 0, 1);
        for(int i=0; i<12; i++){
            lista.add(getMonthAmount(datum, oznaka));
            c.setTime(datum);
            c.add(Calendar.MONTH, 1);
            datum = c.getTime();
        }
        if(oznaka == 1){
            for(int i=1; i<lista.size(); i++){
                lista.set(i, lista.get(i-1)+lista.get(i));
            }
        }
        return lista;
    }

    @Override
    public void onDone(ArrayList<Transaction> results) {
        transactions = results;
        view.setTransactions();
    }
}
