package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TransactionListInteractor extends AsyncTask<Void, Integer, Void> implements ITransactionListInteractor {
    private String id = "9a575e28-e19d-442b-b946-5ebad091e114";
    private String root = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    ArrayList<Transaction> transactions;
    private OnTransactionsSearchDone caller;


    public TransactionListInteractor(TransactionListInteractor.OnTransactionsSearchDone caller) {
        this.caller = caller;
        transactions = new ArrayList<Transaction>();
    }

    public TransactionListInteractor() {

    }


    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    public interface OnTransactionsSearchDone {
        public void onDone(ArrayList<Transaction> results);
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(transactions);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        for(int i=0; ; i++) {
            String url1 = root + "/account/" + id + "/transactions/filter?page=" + i;
            try {
                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setConnectTimeout(5000);
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String result = convertStreamToString(in);
                JSONObject jo = new JSONObject(result);
                JSONArray results = jo.getJSONArray("transactions");
                if(results.length()==0) break;
                System.out.println("broj stranica je : " + result.length());
                for (int j = 0; j < results.length(); j++) {
                    JSONObject movie = results.getJSONObject(j);
                    String title = movie.getString("title");
                    Integer id = movie.getInt("id");
                    String date = movie.getString("date");
                    Double amount = movie.getDouble("amount");
                    Integer type = movie.getInt("TransactionTypeId");
                    String itemDescription = movie.getString("itemDescription");
                    String transactionInterval = movie.getString("transactionInterval");
                    String endDate = movie.getString("endDate");
                    if(validacija(date, amount, title, type, transactionInterval, endDate)){
                        transactions.add(new Transaction(id, date, amount, title, type, itemDescription, transactionInterval, endDate));
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");


    static int broj = 0;
    private boolean validacija(String date, Double amount, String title, Integer tip, String interval, String endDate){
        if(title.length()<3 || title.length()>15) return false;
        if(tip == 1 || tip == 2){
            if(interval.equals("null") || endDate.equals("null")) return false;
            try {
                Date date1 = format.parse(date);
                Date date2 = format.parse(endDate);
                if(date1.after(date2)) return false;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(tip == 3 || tip == 4 || tip == 5){
            if(!(interval.equals("null") || endDate.equals("null"))) return false;
        }

        return true;
    }

    @Override
    public ArrayList<Transaction> getTransaction() {
        if(TransactionsModel.transactions.size() != 0){
            return TransactionsModel.transactions;
        } else {
            return null;
        }

    }

    @Override
    public ArrayList<String> getTransactionType() {
        return TransactionsModel.types;
    }

    @Override
    public ArrayList<String> getSortType() {
        return TransactionsModel.sortTypes;
    }

    @Override
    public void modifikacija(Transaction transaction, Date date, Double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate){
        transaction.setDate(popraviVrijeme(date));
        transaction.setAmount(amount);
        transaction.setTitle(title);
        transaction.setType(type);
        transaction.setItemDescription(itemDescription);
        transaction.setTransactionInterval(transactionInterval);
        transaction.setEndDate(endDate);
    }

    private Date popraviVrijeme(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -1);
        return c.getTime();
    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        TransactionsModel.transactions = transactions;
    }

    @Override
    public void deleteTransaction(Transaction t){
        TransactionsModel.transactions.remove(t);
    }

    @Override
    public void addTransaction(Transaction t) {
        TransactionsModel.transactions.add(t);
    }
}
