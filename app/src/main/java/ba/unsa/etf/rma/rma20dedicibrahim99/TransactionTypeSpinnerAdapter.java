package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class TransactionTypeSpinnerAdapter extends ArrayAdapter<String> {

    public TransactionTypeSpinnerAdapter(Context context, ArrayList<String> countryList) {
        super(context, 0, countryList);
    }

    public void setTransactionType (ArrayList<String> transactionType) {
        this.addAll(transactionType);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_element, parent, false
            );
        }

        ImageView imageViewFlag = convertView.findViewById(R.id.imageView4);
        TextView textViewName = convertView.findViewById(R.id.textView6);

        String currentItem = getItem(position);

        if (currentItem != null) {
            textViewName.setText(currentItem);
            String typeMatch = currentItem.replaceAll("\\s+","_").toLowerCase();
            try {
                Class res = R.drawable.class;
                Field field = res.getField(typeMatch);
                int drawableId = field.getInt(null);
                imageViewFlag.setImageResource(drawableId);
            }
            catch (Exception e) {
                imageViewFlag.setVisibility(View.GONE);
            }

        }

        return convertView;
    }


}

