package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

public class PostTransaction extends AsyncTask<String, Integer, Void> {
    private String id = "9a575e28-e19d-442b-b946-5ebad091e114";
    private String root = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com";
    private Account account;
    private OnPostTransactionDone caller;

    public PostTransaction(OnPostTransactionDone caller) {
        this.caller = caller;
    }

    public interface OnPostTransactionDone {
        public void onDone();
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone();
    }


    @Override
    protected Void doInBackground(String... strings) {
        System.out.println("u bgr" + strings[1]);
        String url1 = root + "/account/" + id + "/transactions";
        if(!strings[0].equals("-9876")) {
            url1 = url1 +"/" + strings[0];
        }
        try {
            URL url = new URL(url1);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(5000);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            String jsonInputString;
            jsonInputString = "{\"date\":" + strings[1] +
                    ",\"title\":" + strings[3] +
                    ",\"amount\":" + strings[2] +
                    ",\"itemDescription\":" + strings[5] +
                    ",\"transactionInterval\":" + strings[6] +
                    ",\"endDate\":" + strings[7] +
                    ",\"TransactionTypeId\":" + strings[4] +"}";
            System.out.println(url1);
            System.out.println(jsonInputString);
            try(OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            System.out.println(con.getResponseCode());
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
