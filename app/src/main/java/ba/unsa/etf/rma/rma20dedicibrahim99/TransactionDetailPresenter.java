package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.text.BoringLayout;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class TransactionDetailPresenter implements ITransactionDetailPresenter, DeleteTransaction.OnDeleteTransactionDone, PostTransaction.OnPostTransactionDone {

    NetworkInfo networkInfo;
    ConnectivityManager connMgr;
    private Context context;
    private ITransactionListInteractor interactor;
    private Transaction transaction;
    private IAccountInteractor iAccountInteractor;
    private DeleteTransaction deleteTransaction;

    public TransactionDetailPresenter( Context context) {

        this.context = context;
        iAccountInteractor = new AccountInteractor();
        interactor = new TransactionListInteractor();
    }




    @Override
    public void create(Date date, Double amount, String title, TransactionType type, String itemDescription, int transactionInterval, Date endDate) {
        for(Transaction t : interactor.getTransaction()){
            if(type.toString().substring(0,7).equals("Regular") && t.getDate().compareTo(date)==0 && (Double.compare(t.getAmount(), amount) == 0) && t.getTitle().equals(title) &&
                    t.getType().toString().equals(type.toString()) &&
                    (itemDescription == null || t.getItemDescription().equals(itemDescription))
                    && (Double.compare(t.getTransactionInterval(), transactionInterval)==0) && (t.getEndDate().compareTo(endDate) == 0)){
                transaction = t;

                return;
            }
            else if(t.getDate().compareTo(date)==0 && (Double.compare(t.getAmount(), amount) == 0) && t.getTitle().equals(title) &&
                    t.getType().toString().equals(type.toString()) &&
                    (itemDescription == null || t.getItemDescription().equals(itemDescription))){
                transaction = t;

                return;
            }
        }
        transaction = new Transaction();

    }

    boolean between(int i, int min, int max){
        return (i>= min && i<=max);
    }

    @Override
    public Double getMonthAmount(Date datum, int oznaka){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datum);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        Date lastDayOfMonth = calendar.getTime();
        Date firstDayOfMonth = new Date(datum.getYear(), datum.getMonth(), 1);
        Date datum1 = null;
        Date datum2 = null;
        Double rez = 0.0;
        for (Transaction t : interactor.getTransaction()) {
            if(oznaka == 0 && t == transaction) continue;
            if(oznaka == 2 && !t.getType().toString().contains("income")) continue;
            if(oznaka == 3 && t.getType().toString().contains("income")) continue;
            if((t.getDate().getMonth() == datum.getMonth() && t.getDate().getYear() == datum.getYear() )||
                    ((t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income"))
                            && between(datum.getMonth(), t.getDate().getMonth(), t.getEndDate().getMonth()) &&
                            between(datum.getYear(), t.getDate().getYear(), t.getEndDate().getYear()))){
                Double amount = t.getAmount();
                if(!t.getType().toString().contains("income")){
                    amount *= -1;
                }
                if(t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income")){
                    datum1 = firstDayOfMonth;
                    datum2 = lastDayOfMonth;
                    int brDana = 0;
                    Calendar c = Calendar.getInstance();
                    Date pocetak = new Date(String.valueOf(t.getDate()));
                    while(pocetak.before(t.getEndDate())){
                        if(pocetak.after(datum1) && pocetak.before(datum2) || pocetak.compareTo(datum1)==0 || pocetak.compareTo(datum2)==0) brDana++;
                        c.setTime(pocetak);
                        c.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
                        pocetak = c.getTime();
                    }
                    rez = rez +(brDana*amount);
                } else {
                    rez += amount;
                }
            }
        }
        return rez;
    }


    @Override
    public Double getTotalAmount() {
        Double rez = 0.0;
        for (Transaction t : interactor.getTransaction()) {
            if(t == transaction) continue;
            Double amount = t.getAmount();
            if(!t.getType().toString().contains("income")){
                amount *= -1;
            }
            if (t.getType().toString().equals("Regular payment") || t.getType().toString().equals("Regular income")) {
                long brDana = t.getEndDate().getTime() - t.getDate().getTime();
                brDana = TimeUnit.DAYS.convert(brDana, TimeUnit.MILLISECONDS);
                brDana++;
                brDana = brDana / t.getTransactionInterval();
                rez = rez + brDana * amount;
            } else {
                rez += amount;
            }
        }
        return rez;
    }

    @Override
    public Double getMonthLimit() {
        return iAccountInteractor.getAccount().getMonthLimit();
    }

    @Override
    public Double getTotalLimit() {
        return iAccountInteractor.getAccount().getTotalLimit();
    }

    @Override
    public void update(boolean oznaka, Date date, Double amount, String title, TransactionType type, String itemDescription, Integer transactionInterval, Date endDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        if (oznaka == false) {
            interactor.modifikacija(transaction, date, amount, title, type, itemDescription, transactionInterval, endDate);
            String endDate1;
            String interval;
            if (type.toId().equals("4") || type.toId().equals("3") || type.toId().equals("5")) {
                endDate1 = null;
                interval = null;
            } else {
                Calendar c1 = Calendar.getInstance();
                c1.setTime(endDate);
                c1.add(Calendar.DATE, 1);
                endDate = c1.getTime();
                endDate1 =  endDate.toString() ;
                interval =  transactionInterval.toString() ;
            }
            connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new PostTransaction((PostTransaction.OnPostTransactionDone) this).execute(
                        "-9876",  "\""+date.toString()+ "\"", amount.toString(), "\""+title+ "\"", type.toId(), "\""+itemDescription+ "\"", "\""+interval+ "\"", "\""+endDate1+ "\"");
            } else {
                ContentValues values = new ContentValues();
                transaction.setId(-9876);
                values.put(TransactionsProvider.ID, "-9876");
                values.put(TransactionsProvider.DATE,date.toString());
                values.put(TransactionsProvider.AMOUNT,amount.toString());
                values.put(TransactionsProvider.TITLE,title);
                values.put(TransactionsProvider.TYPEID,type.toId());
                values.put(TransactionsProvider.ITEMDESCRIPTION,itemDescription);
                values.put(TransactionsProvider.INTERVAL,interval);
                values.put(TransactionsProvider.ENDDATE,endDate1);
                Uri uri = context.getContentResolver().insert(TransactionsProvider.CONTENT_URI, values);
                transaction.setIdBaze(uri.toString().substring(74));
            }
        } else {
            interactor.modifikacija(transaction, date, amount, title, type, itemDescription, transactionInterval, endDate);
            String endDate1;
            String interval;
            if (type.toId().equals("4") || type.toId().equals("3") || type.toId().equals("5")) {
                endDate1 = null;
                interval = null;
            } else {
                Calendar c1 = Calendar.getInstance();
                c1.setTime(endDate);
                c1.add(Calendar.DATE, 1);
                endDate = c1.getTime();
                endDate1 =  endDate.toString() ;
                interval = transactionInterval.toString();
            }
            connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new PostTransaction((PostTransaction.OnPostTransactionDone) this).execute(
                        transaction.getId().toString(), "\""+date.toString()+ "\"", amount.toString(), "\""+title+ "\"", type.toId(), "\""+itemDescription+ "\"", "\""+ interval  + "\"", "\""+endDate1+ "\"");
            } else {
                if(transaction.getIdBaze() == null) {
                    ContentValues values = new ContentValues();
                    values.put(TransactionsProvider.ID, transaction.getId());
                    values.put(TransactionsProvider.DATE, date.toString());
                    values.put(TransactionsProvider.AMOUNT, amount.toString());
                    values.put(TransactionsProvider.TITLE, title);
                    values.put(TransactionsProvider.TYPEID, type.toId());
                    values.put(TransactionsProvider.ITEMDESCRIPTION, itemDescription);
                    values.put(TransactionsProvider.INTERVAL, interval);
                    values.put(TransactionsProvider.ENDDATE, endDate1);
                    Uri uri = context.getContentResolver().insert(TransactionsProvider.CONTENT_URI, values);
                    transaction.setIdBaze(uri.toString().substring(74));
                } else {
                    ContentValues values = new ContentValues();
                    Uri uri = Uri.parse(TransactionsProvider.URL + "/" + transaction.getIdBaze());
                    values.put(TransactionsProvider.ID, transaction.getId());
                    values.put(TransactionsProvider.DATE, date.toString());
                    values.put(TransactionsProvider.AMOUNT, amount.toString());
                    values.put(TransactionsProvider.TITLE, title);
                    values.put(TransactionsProvider.TYPEID, type.toId());
                    values.put(TransactionsProvider.ITEMDESCRIPTION, itemDescription);
                    values.put(TransactionsProvider.INTERVAL, interval);
                    values.put(TransactionsProvider.ENDDATE, endDate1);
                    context.getContentResolver().update(uri, values, null, null);
                }
            }
        }
    }

    public void postanjeTransakcija(){
        String URL = "content://ba.unsa.etf.rma.rma20dedicibrahim99transactions.CP/transactions";
        Uri transactions = Uri.parse(URL);
        Cursor c = context.getContentResolver().query(transactions, null, null, null, "_id");
        if(c.moveToNext()){
            do{
                System.out.println("api datum " + c.getString(c.getColumnIndex(TransactionsProvider.DATE)));
                new PostTransaction((PostTransaction.OnPostTransactionDone) this).execute(
                        c.getString(c.getColumnIndex(TransactionsProvider.ID)),
                        "\""+c.getString(c.getColumnIndex(TransactionsProvider.DATE))+ "\"",
                        c.getString(c.getColumnIndex(TransactionsProvider.AMOUNT)),
                        "\""+c.getString(c.getColumnIndex(TransactionsProvider.TITLE))+ "\"",
                        c.getString(c.getColumnIndex(TransactionsProvider.TYPEID)),
                        "\""+c.getString(c.getColumnIndex(TransactionsProvider.ITEMDESCRIPTION))+ "\"",
                        "\""+ c.getString(c.getColumnIndex(TransactionsProvider.INTERVAL))  + "\"",
                        "\""+c.getString(c.getColumnIndex(TransactionsProvider.ENDDATE))+ "\"");
            } while (c.moveToNext());
        }
        context.getContentResolver().delete(TransactionsProvider.CONTENT_URI, null, null);

    }

    public void brisanjeTransakcija(){
        String URL = "content://ba.unsa.etf.rma.rma20dedicibrahim99transactionsdel.CP/transactions";
        Uri transactions = Uri.parse(URL);
        Cursor c = context.getContentResolver().query(transactions, null, null, null, "_id");
        if(c.moveToNext()){
            do{
                new DeleteTransaction((DeleteTransaction.OnDeleteTransactionDone)this).execute(Integer.parseInt(c.getString(c.getColumnIndex(TransactionsDeleteProvider.ID))));
            } while (c.moveToNext());
        }
        context.getContentResolver().delete(TransactionsDeleteProvider.CONTENT_URI, null, null);
    }

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    @Override
    public void setTransaction(Parcelable transaction) {
        this.transaction = (Transaction) transaction;
    }


    public ArrayList<String> getTypes(){
        return interactor.getTransactionType();
    }



    @Override
    public void delete(int id) {
        connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DeleteTransaction((DeleteTransaction.OnDeleteTransactionDone)this).execute(id);
        } else {
            if(transaction.getIdDelBaze() == null) {
                ContentValues values = new ContentValues();
                values.put(TransactionsDeleteProvider.ID, transaction.getId().toString());
                values.put(TransactionsDeleteProvider.DATE, transaction.getDate().toString());
                values.put(TransactionsDeleteProvider.AMOUNT, transaction.getAmount().toString());
                values.put(TransactionsDeleteProvider.TITLE, transaction.getTitle());
                values.put(TransactionsDeleteProvider.TYPEID, transaction.getType().toId());
                values.put(TransactionsDeleteProvider.ITEMDESCRIPTION, transaction.getItemDescription() == null ? null : transaction.getItemDescription().toString());
                values.put(TransactionsDeleteProvider.INTERVAL, transaction.getTransactionInterval() == null ? null : transaction.getTransactionInterval().toString());
                values.put(TransactionsDeleteProvider.ENDDATE, transaction.getEndDate() == null ? null : transaction.getEndDate().toString());
                Uri uri = context.getContentResolver().insert(TransactionsDeleteProvider.CONTENT_URI, values);
                transaction.setIdDelBaze(uri.toString().substring(77));
            } else {
                Uri uri = Uri.parse(TransactionsDeleteProvider.URL + "/" + transaction.getIdDelBaze());
                context.getContentResolver().delete(uri, null, null);
                transaction.setIdDelBaze(null);
            }

        }
        //interactor.deleteTransaction(transaction);
        // interactor.getTransaction().remove(transaction);
    }

    @Override
    public void addTransaction() {
        interactor.addTransaction(transaction);
        //interactor.getTransaction().add(transaction);
    }

    @Override
    public void onDone() {

    }
}
