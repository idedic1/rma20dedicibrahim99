package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.UserDictionary;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import static ba.unsa.etf.rma.rma20dedicibrahim99.AccountProvider.TOTALLIMIT;
import static ba.unsa.etf.rma.rma20dedicibrahim99.AccountProvider.URL;


public class BudgetFragment extends Fragment implements IBudgetFragment {

    private TextView bugdetTv;
    private TextView bugdetTv1;
    private TextView totalTv;
    private TextView monthTv;
    private TextView rezimTv;
    private Button saveBtn;
    private EditText totalEt;
    private EditText monthEt;

    private IAccountPresenter presenter;
    Account account;

    public IAccountPresenter getPresenter() {
        if (presenter == null) {
            presenter = new AccountPresenter(this, getActivity());
        }
        return presenter;
    }

    private OnItemTouch onItemTouch;

    NetworkInfo networkInfo;
    ConnectivityManager connMgr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_budget, container, false);


        bugdetTv = (TextView) view.findViewById(R.id.textView9);
        bugdetTv1 = (TextView) view.findViewById(R.id.textView10);
        totalTv = (TextView) view.findViewById(R.id.textView11);
        monthTv = (TextView) view.findViewById(R.id.textView12);
        totalEt = (EditText) view.findViewById(R.id.editText5);
        monthEt = (EditText) view.findViewById(R.id.editText6);
        saveBtn = (Button) view.findViewById(R.id.button5);
        rezimTv = (TextView) view.findViewById(R.id.textView14);

        /*String URL = "content://ba.unsa.etf.rma.rma20dedicibrahim99.CP/accounts";
        Uri students = Uri.parse(URL);
        Cursor c = getContext().getContentResolver().query(students, null, null, null, "budget");
        if (c.moveToFirst()) {
            do {
                Toast.makeText(getActivity(), c.getString(c.getColumnIndex(AccountProvider._ID)) + ", " + c.getString(c.getColumnIndex(AccountProvider.BUDGET)) + ", " + c.getString(c.getColumnIndex(TOTALLIMIT)), Toast.LENGTH_SHORT).show();
            } while (c.moveToNext());
        }*/


        connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            getPresenter().searchAccount();
            rezimTv.setVisibility(View.INVISIBLE);
        } else {
            bugdetTv1.setText(getPresenter().getBudget().toString());
            totalEt.setText(getPresenter().getTotalLimit().toString());
            monthEt.setText(getPresenter().getMonthLimit().toString());
            rezimTv.setText("Offline izmjena");
        }

        saveBtn.setOnClickListener(saveBtnListener);



        onItemTouch = (OnItemTouch) getActivity();
        view.setOnTouchListener(swipeListener);
        return view;
    }


    private OnSwipeTouchListener swipeListener = new OnSwipeTouchListener(getActivity()){
        @Override
        public void onSwipeLeft() {
            onItemTouch.onLeftTouch(2);
        }
        @Override
        public void onSwipeRight() {
            onItemTouch.onRightTocuh(0);
        }
    };

    private View.OnClickListener saveBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                getPresenter().postAccount(bugdetTv1.getText().toString(), totalEt.getText().toString(), monthEt.getText().toString());
            } else {
                getPresenter().updateTotalLimit(Double.parseDouble(totalEt.getText().toString()));
                getPresenter().updateMonthLimit(Double.parseDouble(monthEt.getText().toString()));
                getPresenter().dodajUBazu(bugdetTv1.getText().toString(), totalEt.getText().toString(), monthEt.getText().toString());

            }
        }
    };


    @Override
    public void setAccount(Account account) {
        bugdetTv1.setText(account.getBudget().toString());
        totalEt.setText(account.getTotalLimit().toString());
        monthEt.setText(account.getMonthLimit().toString());
    }

    @Override
    public void notifyDataSetChanged() {
    }
}
