package ba.unsa.etf.rma.rma20dedicibrahim99;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.widget.Toast;

public class ConnectivityBroadcastReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        AccountPresenter presenter = new AccountPresenter(context);
        TransactionDetailPresenter presenter1 = new TransactionDetailPresenter(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null) {
            Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
            toast.show();
        }
        else {
            Toast toast = Toast.makeText(context, "Connected", Toast.LENGTH_SHORT);
            toast.show();
            presenter.prenosPodataka();
            presenter1.postanjeTransakcija();
            presenter1.brisanjeTransakcija();
        }
    }
}