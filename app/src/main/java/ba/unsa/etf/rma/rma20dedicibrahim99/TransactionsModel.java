package ba.unsa.etf.rma.rma20dedicibrahim99;

import java.util.ArrayList;
import java.util.Date;

public class TransactionsModel {
    public static ArrayList<Transaction> transactions = new ArrayList<Transaction>();

    public static ArrayList<String> types = new ArrayList<String>() {
        {
            add("All");
            add("Individual payment");
            add("Regular payment");
            add("Purchase");
            add("Individual income");
            add("Regular income");

        }
    };

    public static ArrayList<String> sortTypes = new ArrayList<String>() {
        {
            add("Price - Ascending");
            add("Price - Descending");
            add("Title - Ascending");
            add("Title - Descending");
            add("Date - Ascending");
            add("Date - Descending");

        }
    };

}
