package ba.unsa.etf.rma.rma20dedicibrahim99;

public interface IAccountPresenter {
    Double getTotalLimit();
    Double getMonthLimit();
    Double getBudget();
    void updateMonthLimit(Double limit);
    void updateTotalLimit(Double limit);
    public void searchAccount();
    public void postAccount(String... strings);
    void dodajUBazu(String toString, String toString1, String toString2);
}
